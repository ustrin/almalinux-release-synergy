Summary: AlmaLinux Synergy repository configuration
Name:    almalinux-release-synergy
Version: 8
Release: 2%{?dist}
License: GPLv2
URL:     https://almalinux.org/
BuildArch: noarch
Source0: almalinux-synergy.repo

Requires:       epel-release
Requires(post): epel-release
Provides: almalinux-release-synergy = 8

%description
DNF configuration for AlmaLinux Synergy repo

%install
install -D -m 644 %{SOURCE0} %{buildroot}%{_sysconfdir}/yum.repos.d/almalinux-synergy.repo

%post
if [ -x /usr/bin/crb ]; then
  /usr/bin/crb enable
fi

%files
%defattr(-,root,root)
%config(noreplace) %{_sysconfdir}/yum.repos.d/almalinux-synergy.repo

%changelog
* Thu Aug 24 2023 Andrew Lukoshko <alukoshko@almalinux.org> - 8-2
- Enable PowerTools/CRB automatically
- Enable countme

* Mon Aug 14 2023 Andrew Lukoshko <alukoshko@almalinux.org> - 8-1
- Initial version
